# OpenRGBRemote

Android app that allows you to change every device color and modes. Based on [OpenRGB Java Client](https://gitlab.com/mguimard/openrgb-client).

Features :

* Scan for OpenRGB servers on network
* Apply a mode to a device
* Change zones color
* Create custom color gradients
* Save/Load/Delete a profile
* Show devices info

## Installation

Requires [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) running in server mode.

Download the apk from the [latest pipeline](https://gitlab.com/mguimard/openrgbremote/-/jobs/artifacts/master/raw/OpenRGBRemote.apk?job=assembleDebug), place it on your phone, then click on it.

(You may need to allow apps from unknown sources in your phone settings)

## Screenshots

![device](screenshot-mb.png)
![gradients](screenshot-gradients.png)

