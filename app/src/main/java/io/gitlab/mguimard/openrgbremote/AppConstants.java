package io.gitlab.mguimard.openrgbremote;

public class AppConstants {
    public static final int DEFAULT_PORT = 6742;
    public static final String DEFAULT_NAME = "OpenRGBRemote";
}
