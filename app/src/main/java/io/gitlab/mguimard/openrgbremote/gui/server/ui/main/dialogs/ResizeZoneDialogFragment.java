package io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;

public class ResizeZoneDialogFragment extends DialogFragment {

    private final int ledMin;
    private final int ledMax;
    private final int ledCount;
    private final ZoneSizeChangeListener listener;

    public ResizeZoneDialogFragment(int ledMin, int ledMax, int ledCount, ZoneSizeChangeListener listener) {
        this.ledMin = ledMin;
        this.ledMax = ledMax;
        this.ledCount = ledCount;
        this.listener = listener;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_change_leds_count, null);

        NumberPicker numberPicker = view.findViewById(R.id.leds_count_value);
        numberPicker.setMinValue(ledMin);
        numberPicker.setMaxValue(ledMax);
        numberPicker.setValue(ledCount);

        builder.setView(view)
                .setPositiveButton("Set", (dialog, id) -> listener.onZoneSizeChanged(numberPicker.getValue()))
                .setNegativeButton("Cancel", (dialog, id) -> ResizeZoneDialogFragment.this.getDialog().cancel());
        return builder.create();
    }

    public interface ZoneSizeChangeListener {
        void onZoneSizeChanged(int value);
    }
}