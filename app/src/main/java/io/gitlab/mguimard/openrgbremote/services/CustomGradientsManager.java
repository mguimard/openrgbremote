package io.gitlab.mguimard.openrgbremote.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.models.CustomGradient;
import io.gitlab.mguimard.openrgbremote.models.CustomGradientList;

public class CustomGradientsManager {

    private static final String PREF_KEY = "CustomGradientsManager";
    private static final String GRADIENTS_ENTRY_KEY = "CustomGradientsList";

    private static CustomGradientsManager instance;
    private final CustomGradientList customGradientList;
    private final SharedPreferences sharedPref;

    private CustomGradientsManager(Context context) {
        // Load form prefs
        sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = sharedPref.getString(GRADIENTS_ENTRY_KEY, null);

        if (null == json) {
            customGradientList = new CustomGradientList(new ArrayList<>());
            saveCustomGradientList();
        } else {
            customGradientList = gson.fromJson(json, CustomGradientList.class);
        }

    }

    public static CustomGradientsManager getInstance(Context context) {
        if (instance == null) {
            instance = new CustomGradientsManager(context);
        }
        return instance;
    }

    public void addCustomGradient(CustomGradient gradient) {
        customGradientList.add(gradient);
        saveCustomGradientList();
    }

    private void saveCustomGradientList() {
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(customGradientList);
        prefsEditor.putString(GRADIENTS_ENTRY_KEY, json);
        prefsEditor.apply();
    }

    public CustomGradientList getCustomGradientList() {
        return customGradientList;
    }

    public boolean isEmpty() {
        return this.customGradientList.isEmpty();
    }

    public void removeCustomGradient(CustomGradient customGradient) {
        customGradientList.remove(customGradient);
        saveCustomGradientList();
    }
}
