package io.gitlab.mguimard.openrgbremote.gui.about;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.BuildConfig;
import io.gitlab.mguimard.openrgbremote.R;

public class AboutDialogFragment extends DialogFragment {

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_about, null);
        TextView versionTextView = view.findViewById(R.id.app_version);

        versionTextView.setText(BuildConfig.VERSION_NAME);

        builder.setView(view)
                .setNegativeButton("Close", (dialog, id) -> AboutDialogFragment.this.getDialog().cancel());

        return builder.create();
    }

}