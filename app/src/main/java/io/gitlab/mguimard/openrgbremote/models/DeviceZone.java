package io.gitlab.mguimard.openrgbremote.models;

import io.gitlab.mguimard.openrgb.entity.OpenRGBDevice;
import io.gitlab.mguimard.openrgb.entity.OpenRGBZone;

public class DeviceZone {

    private final OpenRGBDevice device;
    private final OpenRGBZone zone;
    private final int deviceIndex;
    private final int zoneIndex;

    public DeviceZone(OpenRGBDevice device, OpenRGBZone zone, int deviceIndex, int zoneIndex) {
        this.device = device;
        this.zone = zone;
        this.deviceIndex = deviceIndex;
        this.zoneIndex = zoneIndex;
    }

    public OpenRGBDevice getDevice() {
        return device;
    }

    public OpenRGBZone getZone() {
        return zone;
    }

    public int getDeviceIndex() {
        return deviceIndex;
    }

    public int getZoneIndex() {
        return zoneIndex;
    }
}