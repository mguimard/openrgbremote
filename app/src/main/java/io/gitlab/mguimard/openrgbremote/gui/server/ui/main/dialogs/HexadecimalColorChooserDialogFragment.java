package io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;

public class HexadecimalColorChooserDialogFragment extends DialogFragment {

    private final String current;
    private final HexadecimalColorChooserListener listener;

    public HexadecimalColorChooserDialogFragment(String current, HexadecimalColorChooserListener listener) {
        this.current = current;
        this.listener = listener;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_change_hexa_color, null);
        EditText colorEditText = view.findViewById(R.id.hexa_color_value);

        colorEditText.setText(current);

        builder.setView(view)
                .setPositiveButton("Set", (dialog, id) -> {
                    String newValue = colorEditText.getText().toString();
                    listener.onColorChanged(newValue);
                })
                .setNegativeButton("Cancel", (dialog, id) -> HexadecimalColorChooserDialogFragment.this.getDialog().cancel());
        return builder.create();
    }

    public interface HexadecimalColorChooserListener {
        void onColorChanged(String value);
    }
}