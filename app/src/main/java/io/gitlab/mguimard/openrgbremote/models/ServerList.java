package io.gitlab.mguimard.openrgbremote.models;

import java.util.ArrayList;

public class ServerList {
    private ArrayList<OpenRGBServer> servers;

    public ServerList(ArrayList<OpenRGBServer> servers) {
        this.servers = servers;
    }

    public ArrayList<OpenRGBServer> getServers() {
        return servers;
    }

    public void setServers(ArrayList<OpenRGBServer> servers) {
        this.servers = servers;
    }

    public void add(OpenRGBServer server) {
        if (!servers.contains(server)) {
            servers.add(server);
        }
    }

    public void addAll(ArrayList<OpenRGBServer> servers) {
        for (OpenRGBServer server : servers) {
            add(server);
        }
    }

    public boolean remove(OpenRGBServer server) {
        return this.servers.remove(server);
    }

    public boolean isEmpty() {
        return this.servers.isEmpty();
    }
}