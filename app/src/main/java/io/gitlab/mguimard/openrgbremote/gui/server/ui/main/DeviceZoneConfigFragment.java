package io.gitlab.mguimard.openrgbremote.gui.server.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.flask.colorpicker.ColorPickerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.gitlab.mguimard.openrgb.entity.ColorMode;
import io.gitlab.mguimard.openrgb.entity.OpenRGBColor;
import io.gitlab.mguimard.openrgb.entity.OpenRGBDevice;
import io.gitlab.mguimard.openrgb.entity.OpenRGBMode;
import io.gitlab.mguimard.openrgb.entity.OpenRGBZone;
import io.gitlab.mguimard.openrgb.utils.FlagsChecker;
import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.adapters.ModeListAdapter;
import io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs.GradientColorChooserDialogFragment;
import io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs.HexadecimalColorChooserDialogFragment;
import io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs.ResizeZoneDialogFragment;
import io.gitlab.mguimard.openrgbremote.models.DeviceZone;
import io.gitlab.mguimard.openrgbremote.services.ServerCommandRunner;

public class DeviceZoneConfigFragment extends Fragment {

    private String ip;
    private int port;

    private OpenRGBDevice device;
    private OpenRGBZone zone;
    private int deviceIndex;
    private int zoneIndex;


    public DeviceZoneConfigFragment() {
    }

    public static DeviceZoneConfigFragment newInstance(String ip, int port, DeviceZone deviceZone) {
        DeviceZoneConfigFragment fragment = new DeviceZoneConfigFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        fragment.ip = ip;
        fragment.port = port;

        fragment.device = deviceZone.getDevice();
        fragment.zone = deviceZone.getZone();
        fragment.deviceIndex = deviceZone.getDeviceIndex();
        fragment.zoneIndex = deviceZone.getZoneIndex();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_mode_config, container, false);

        List<OpenRGBMode> modes = device.getModes();
        Spinner modeSpinner = root.findViewById(R.id.mode_selector);

        ModeListAdapter modesAdapter = new ModeListAdapter(getContext(), R.layout.mode_spinner_item, R.id.mode_name, new ArrayList<>(modes));
        modeSpinner.setAdapter(modesAdapter);
        modeSpinner.setSelection(device.getActiveMode(), false);

        modeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OpenRGBMode mode = device.getModes().get(position);
                ServerCommandRunner.setMode(ip, port, deviceIndex, position, mode);
                device.setActiveMode(position);
                onModeChanged(root);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        onModeChanged(root);
        return root;
    }

    private void onModeChanged(View root) {

        LinearLayout colorConfigArea = root.findViewById(R.id.color_config_area);
        ConstraintLayout speedConfigArea = root.findViewById(R.id.speed_config_area);

        Button changeLEDCountButton = root.findViewById(R.id.change_leds_count);
        ColorPickerView colorPickerView = root.findViewById(R.id.color_picker_view);
        Button colorChooserButton = root.findViewById(R.id.hexa_color_chooser);
        Button gradientColorChooserButton = root.findViewById(R.id.gradient_color_chooser);
        SeekBar speedSeekBar = root.findViewById(R.id.speed_seekbar);

        OpenRGBMode mode = device.getModes().get(device.getActiveMode());
        ColorMode colorMode = mode.getColorMode();

        FlagsChecker flags = new FlagsChecker(mode.getFlags());

        boolean showColorsArea = colorMode == ColorMode.MODE_COLORS_PER_LED || colorMode == ColorMode.MODE_COLORS_MODE_SPECIFIC;
        boolean showSpeedArea = flags.hasSpeed();
        // boolean showGradientButton = colorMode == ColorMode.MODE_COLORS_PER_LED;
        boolean showLEDArea = zone.getLedsMax() > zone.getLedsMin();

        /*
         *     -------------- LEDs --------------
         */
        if (showLEDArea) {
            changeLEDCountButton.setVisibility(View.VISIBLE);
            String text = "LEDs: " + zone.getLedsCount();
            changeLEDCountButton.setText(text);

            changeLEDCountButton.setOnClickListener(v -> {

                DialogFragment fragment = new ResizeZoneDialogFragment(zone.getLedsMin(), zone.getLedsMax(), zone.getLedsCount(), value -> {
                    zone.setLedsCount(value);
                    String updatedText = "LEDs: " + zone.getLedsCount();
                    changeLEDCountButton.setText(updatedText);
                    ServerCommandRunner.resizeZone(ip, port, deviceIndex, zoneIndex, value);
                    gradientColorChooserButton.setVisibility(value > 1 ? View.VISIBLE : View.GONE);
                });

                fragment.setTargetFragment(this, 1);
                fragment.show(getParentFragmentManager(), null);
            });
        } else {
            changeLEDCountButton.setVisibility(View.GONE);
        }



        /*
         *     -------------- COLORS --------------
         */
        if (showColorsArea) {
            colorConfigArea.setVisibility(View.VISIBLE);
            OpenRGBColor firstColor = null;

            if (!device.getColors().isEmpty()) {
                firstColor = device.getColors().get(0);
            } else if (!mode.getColors().isEmpty()) {
                firstColor = mode.getColors().get(0);
            }

            if (null != firstColor) {
                byte red = firstColor.getRed();
                byte green = firstColor.getGreen();
                byte blue = firstColor.getBlue();
                colorChooserButton.setText(String.format("#%02X%02X%02X", red, green, blue));
            }

            colorPickerView.addOnColorChangedListener(selectedColor -> {
                colorChooserButton.setText(String.format("#%06X", (0xFFFFFF & selectedColor)));
                OpenRGBColor newColor = OpenRGBColor.fromInt(selectedColor);
                applyColor(newColor);
            });

            colorChooserButton.setOnClickListener(v -> {
                DialogFragment fragment = new HexadecimalColorChooserDialogFragment(colorChooserButton.getText().toString(), value -> {
                    OpenRGBColor newColor = OpenRGBColor.fromHexaString(value);
                    String hexadecimalString = newColor.getHexaString();
                    colorChooserButton.setText(hexadecimalString);
                    applyColor(newColor);
                });

                fragment.setTargetFragment(this, 1);
                fragment.show(getParentFragmentManager(), null);
            });


            gradientColorChooserButton.setOnClickListener(v -> {
                DialogFragment fragment = new GradientColorChooserDialogFragment(zone.getLedsCount(), this::applyColors);
                fragment.setTargetFragment(this, 1);
                fragment.show(getParentFragmentManager(), null);
            });

           /* gradientGeneratorButton.setOnClickListener(v -> {
                DialogFragment fragment = new GradientGeneratorDialogFragment();
                fragment.setTargetFragment(this, 1);
                fragment.show(getParentFragmentManager(), null);
            });*/

            gradientColorChooserButton.setVisibility(zone.getLedsCount() > 1 ? View.VISIBLE : View.GONE);

        } else {
            colorConfigArea.setVisibility(View.GONE);
        }


        /*
         *     -------------- SPEED --------------
         */
        if (showSpeedArea) {
            speedConfigArea.setVisibility(View.VISIBLE);
            int speedMin = Math.min(mode.getSpeedMax(), mode.getSpeedMin());
            int speedMax = Math.max(mode.getSpeedMax(), mode.getSpeedMin());
            int speedDiff = speedMax - speedMin;

            boolean inverted = mode.getSpeedMax() < mode.getSpeedMin();

            if (speedDiff > 0) {
                int initialProgress = (int) (1.0 * mode.getSpeed() / speedDiff * 100);
                if (inverted) {
                    initialProgress = 100 - initialProgress;
                }
                System.out.println(mode);
                System.out.println(initialProgress);
                speedSeekBar.setProgress(initialProgress);
            }

            speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (inverted) {
                        progress = 100 - progress;
                    }
                    int newSpeed = (int) (speedMin + speedDiff * progress / 100.0);
                    mode.setSpeed(newSpeed);
                    ServerCommandRunner.setMode(ip, port, deviceIndex, device.getActiveMode(), mode);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        } else {
            speedConfigArea.setVisibility(View.GONE);
        }
    }

    private void applyColors(OpenRGBColor[] newColors) {
        switch (device.getModes().get(device.getActiveMode()).getColorMode()) {
            case MODE_COLORS_PER_LED:
                applyPerLEDs(newColors);
                break;

            case MODE_COLORS_MODE_SPECIFIC:
                applyModeSpecific(newColors);
                break;

            default:
                break;
        }
    }

    private void applyColor(OpenRGBColor newColor) {
        switch (device.getModes().get(device.getActiveMode()).getColorMode()) {
            case MODE_COLORS_PER_LED:
                applyPerLEDs(newColor);
                break;

            case MODE_COLORS_MODE_SPECIFIC:
                applyModeSpecific(newColor);
                break;

            default:
                break;
        }
    }

    private void applyModeSpecific(OpenRGBColor newColor) {
        OpenRGBMode mode = device.getModes().get(device.getActiveMode());
        OpenRGBColor[] newColors = new OpenRGBColor[device.getLeds().size()];
        Arrays.fill(newColors, newColor);
        applyModeSpecific(newColors);
    }

    private void applyModeSpecific(OpenRGBColor[] newColors) {
        OpenRGBMode mode = device.getModes().get(device.getActiveMode());
        mode.setColors(Arrays.asList(newColors));
        ServerCommandRunner.setMode(ip, port, deviceIndex, device.getActiveMode(), mode);
    }

    private void applyPerLEDs(OpenRGBColor newColor) {
        OpenRGBColor[] newColors = new OpenRGBColor[zone.getLedsCount()];
        Arrays.fill(newColors, newColor);
        applyPerLEDs(newColors);
    }

    private void applyPerLEDs(OpenRGBColor[] newColors) {
        ServerCommandRunner.setZoneLEDs(ip, port, deviceIndex, zoneIndex, newColors);
    }

}