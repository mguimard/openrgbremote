package io.gitlab.mguimard.openrgbremote.models;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

public class OpenRGBServer {

    @NonNull
    String ip;
    int port;

    public OpenRGBServer(@NonNull String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    @NotNull
    public String getIp() {
        return ip;
    }

    public void setIp(@NotNull String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDisplayName() {
        return ip + ":" + port;
    }

    @Override
    public int hashCode() {
        return port + ip.hashCode();
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof OpenRGBServer)) {
            return false;
        }

        OpenRGBServer c = (OpenRGBServer) o;

        return c.getDisplayName().equals(getDisplayName());
    }
}
