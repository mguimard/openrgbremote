package io.gitlab.mguimard.openrgbremote.gui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.adapters.ServerListAdapter;
import io.gitlab.mguimard.openrgbremote.gui.about.AboutDialogFragment;
import io.gitlab.mguimard.openrgbremote.gui.custom.CustomGradientsActivity;
import io.gitlab.mguimard.openrgbremote.gui.server.ServerActivity;
import io.gitlab.mguimard.openrgbremote.models.OpenRGBServer;
import io.gitlab.mguimard.openrgbremote.services.ServerManager;
import io.gitlab.mguimard.openrgbremote.services.SubnetScanner;

public class HomeActivity extends AppCompatActivity
        implements AddServerFragment.AddServerListener {

    ServerManager serverManager;
    SubnetScanner scanner;
    ListView serverListView;
    ServerListAdapter serverListAdapter;
    View serverListNotEmptyView;
    View serverListEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        serverManager = ServerManager.GetInstance(this);

        serverListNotEmptyView = findViewById(R.id.not_empty_server_list);
        serverListEmptyView = findViewById(R.id.empty_server_list);

        toogleListViews();

        FloatingActionButton addServerButton = findViewById(R.id.add_server);
        addServerButton.setOnClickListener(view -> {
            DialogFragment fragment = new AddServerFragment();
            fragment.show(getSupportFragmentManager(), null);
        });

        serverListAdapter = new ServerListAdapter(this, 0, serverManager.getServerList().getServers());
        serverListView = findViewById(R.id.server_list_view);
        serverListView.setAdapter(serverListAdapter);

        serverListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(HomeActivity.this, ServerActivity.class);
            OpenRGBServer server = serverListAdapter.getServer(position);
            intent.putExtra(ServerActivity.IP_EXTRA, server.getIp());
            intent.putExtra(ServerActivity.PORT_EXTRA, server.getPort());
            startActivity(intent);
        });

        serverListView.setOnItemLongClickListener((parent, view, position, id) -> {
            serverManager.removeServer(serverListAdapter.getServer(position));
            updateServerList();
            return false;
        });

        scanner = SubnetScanner.getInstance(this);
        scanner.subscribe((o, arg) -> {
            ArrayList<OpenRGBServer> servers = (ArrayList<OpenRGBServer>) arg;
            serverManager.addServers(servers);
            snack(servers.size() + " server(s) found");
            updateServerList();
        });

    }

    private void toogleListViews() {
        boolean isEmpty = serverManager.getServerList().isEmpty();
        serverListNotEmptyView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        serverListEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan_subnet:
                try {
                    snack("Scanning...");
                    scanner.scan();
                } catch (SubnetScanner.AlreadyScanningException e) {
                    snack("Scanning process already running");
                } catch (SubnetScanner.WifiNotConnectedException e) {
                    snack("Please enable and connect to Wifi");
                }
                return true;

            /*case R.id.action_settings:
                return true;*/

            case R.id.action_about:
                DialogFragment fragment = new AboutDialogFragment();
                fragment.show(getSupportFragmentManager(), null);
                return true;

            case R.id.action_custom_gradients:
                Intent intent = new Intent(HomeActivity.this, CustomGradientsActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onServerAdded(OpenRGBServer server) {
        serverManager.addServer(server);
        updateServerList();
    }

    private void updateServerList() {
        runOnUiThread(() -> {
            toogleListViews();
            serverListAdapter.notifyDataSetChanged();
        });
    }

    private void snack(String text) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentLayout, text, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


}