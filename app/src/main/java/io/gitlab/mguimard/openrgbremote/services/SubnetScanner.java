package io.gitlab.mguimard.openrgbremote.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.stealthcopter.networktools.SubnetDevices;
import com.stealthcopter.networktools.subnet.Device;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import io.gitlab.mguimard.openrgb.client.OpenRGBClient;
import io.gitlab.mguimard.openrgbremote.AppConstants;
import io.gitlab.mguimard.openrgbremote.models.OpenRGBServer;

public class SubnetScanner {

    private static SubnetScanner instance;
    private final Scan scan;
    private boolean isScanning = false;
    private Context context;

    private SubnetScanner() {
        this.scan = new Scan();
    }

    public static SubnetScanner getInstance(Context context) {
        if (null == instance) {
            instance = new SubnetScanner();
            instance.context = context;
        }
        return instance;
    }

    public void subscribe(Observer o) {
        scan.addObserver(o);
    }

    private boolean isConnectedToWifi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifi.isConnected();
    }

    public void scan() throws AlreadyScanningException, WifiNotConnectedException {

        if (!isConnectedToWifi()) {
            throw new WifiNotConnectedException();
        }

        if (this.isScanning) {
            throw new AlreadyScanningException();
        }

        isScanning = true;

        SubnetDevices.fromLocalAddress().findDevices(new SubnetDevices.OnSubnetDeviceFound() {

            @Override
            public void onDeviceFound(Device device) {
            }

            @Override
            public void onFinished(ArrayList<Device> devicesFound) {
                ArrayList<OpenRGBServer> servers = new ArrayList<>();

                for (Device device : devicesFound) {
                    OpenRGBClient client = new OpenRGBClient(device.ip, AppConstants.DEFAULT_PORT, AppConstants.DEFAULT_NAME);
                    try {
                        client.connect();
                        client.disconnect();
                        servers.add(new OpenRGBServer(device.ip, AppConstants.DEFAULT_PORT));
                    } catch (IOException e) {
                        System.out.println("Ignoring device " + device);
                    }
                }

                scan.onFinished(servers);
                isScanning = false;
            }

        });

    }

    static class Scan extends Observable {
        public void onFinished(ArrayList<OpenRGBServer> servers) {
            setChanged();
            notifyObservers(servers);
        }
    }

    public static class AlreadyScanningException extends Throwable {
    }

    public static class WifiNotConnectedException extends Throwable {
    }

}
