package io.gitlab.mguimard.openrgbremote.models;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import io.gitlab.mguimard.openrgb.entity.OpenRGBColor;
import io.gitlab.mguimard.openrgb.utils.ColorUtils;

public class CustomGradient {

    public static final CustomGradient.HSV red = new CustomGradient.HSV(0, 1, 1);
    public static final CustomGradient.HSV vermilion = new CustomGradient.HSV(15, 1, 1);
    public static final CustomGradient.HSV orange = new CustomGradient.HSV(30, 1, 1);
    public static final CustomGradient.HSV goldenYellow = new CustomGradient.HSV(45, 1, 1);
    public static final CustomGradient.HSV yellow = new CustomGradient.HSV(60, 1, 1);
    public static final CustomGradient.HSV yellowishGreen = new CustomGradient.HSV(75, 1, 1);
    public static final CustomGradient.HSV chartreuse = new CustomGradient.HSV(90, 1, 1);
    public static final CustomGradient.HSV leafGreen = new CustomGradient.HSV(105, 1, 1);
    public static final CustomGradient.HSV green = new CustomGradient.HSV(120, 1, 1);
    public static final CustomGradient.HSV cobaltGreen = new CustomGradient.HSV(135, 1, 1);
    public static final CustomGradient.HSV emeraldGreen = new CustomGradient.HSV(150, 1, 1);
    public static final CustomGradient.HSV turquoiseGreen = new CustomGradient.HSV(165, 1, 1);
    public static final CustomGradient.HSV turquoiseBlue = new CustomGradient.HSV(180, 1, 1);
    public static final CustomGradient.HSV ceruleanBlue = new CustomGradient.HSV(195, 1, 1);
    public static final CustomGradient.HSV azure = new CustomGradient.HSV(210, 1, 1);
    public static final CustomGradient.HSV cobaltBlue = new CustomGradient.HSV(225, 1, 1);
    public static final CustomGradient.HSV blue = new CustomGradient.HSV(240, 1, 1);
    public static final CustomGradient.HSV hyacinth = new CustomGradient.HSV(255, 1, 1);
    public static final CustomGradient.HSV violet = new CustomGradient.HSV(270, 1, 1);
    public static final CustomGradient.HSV purple = new CustomGradient.HSV(285, 1, 1);
    public static final CustomGradient.HSV magenta = new CustomGradient.HSV(300, 1, 1);
    public static final CustomGradient.HSV reddishPurple = new CustomGradient.HSV(315, 1, 1);
    public static final CustomGradient.HSV crimson = new CustomGradient.HSV(330, 1, 1);
    public static final CustomGradient.HSV carmine = new CustomGradient.HSV(345, 1, 1);

    private HSV startHSV;
    private HSV endHSV;
    private int waves;

    public CustomGradient(HSV startHSV, HSV endHSV, int waves) {
        this.startHSV = startHSV;
        this.endHSV = endHSV;
        this.waves = waves;
    }

    public HSV getStartHSV() {
        return startHSV;
    }

    public void setStartHSV(HSV startHSV) {
        this.startHSV = startHSV;
    }

    public HSV getEndHSV() {
        return endHSV;
    }

    public void setEndHSV(HSV endHSV) {
        this.endHSV = endHSV;
    }

    public int getWaves() {
        return waves;
    }

    public void setWaves(int waves) {
        this.waves = waves;
    }

    public OpenRGBColor[] toColors(int size) {
        return ColorUtils.generateRainbow(size,
                startHSV.getH(), startHSV.getS(), startHSV.getV(),
                endHSV.getH(), endHSV.getS(), endHSV.getV(),
                waves
        );

    }

    public Bitmap getBitmap(int size) {

        OpenRGBColor[] rainbow = toColors(size);
        Bitmap bitmap = Bitmap.createBitmap(rainbow.length, 1, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();

        for (int i = 0; i < rainbow.length; i++) {
            Rect rectangle = new Rect(i, 0, i + 1, 1);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(rainbow[i].getIntValue());
            paint.setAntiAlias(false);
            canvas.drawRect(rectangle, paint);
        }

        return bitmap;
    }

    public static class HSV {
        int h;
        double s;
        double v;

        public HSV(int h, double s, double v) {
            this.h = h;
            this.s = s;
            this.v = v;
        }

        public int getH() {
            return h;
        }

        public void setH(int h) {
            this.h = h;
        }

        public double getS() {
            return s;
        }

        public void setS(double s) {
            this.s = s;
        }

        public double getV() {
            return v;
        }

        public void setV(double v) {
            this.v = v;
        }
    }

}
