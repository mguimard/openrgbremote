package io.gitlab.mguimard.openrgbremote.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.models.OpenRGBServer;
import io.gitlab.mguimard.openrgbremote.models.ServerList;

public class ServerManager {

    private static final String PREF_KEY = "ServerManager";
    private static final String SERVERS_ENTRY_KEY = "ServerList";

    private static ServerManager instance;
    private final ServerList serverList;
    private final SharedPreferences sharedPref;

    private ServerManager(Context context) {
        // Load form prefs
        sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = sharedPref.getString(SERVERS_ENTRY_KEY, null);

        if (null == json) {
            serverList = new ServerList(new ArrayList<>());
            saveServerList();
        } else {
            serverList = gson.fromJson(json, ServerList.class);
        }

    }

    public static ServerManager GetInstance(Context context) {
        if (instance == null) {
            instance = new ServerManager(context);
        }
        return instance;
    }

    public void addServer(OpenRGBServer server) {
        serverList.add(server);
        saveServerList();
    }

    public void addServers(ArrayList<OpenRGBServer> servers) {
        serverList.addAll(servers);
        saveServerList();
    }

    private void saveServerList() {
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(serverList);
        prefsEditor.putString(SERVERS_ENTRY_KEY, json);
        prefsEditor.apply();
    }

    public ServerList getServerList() {
        return serverList;
    }

    public boolean isEmpty() {
        return this.serverList.isEmpty();
    }

    public void removeServer(OpenRGBServer server) {
        serverList.remove(server);
        saveServerList();
    }
}
