package io.gitlab.mguimard.openrgbremote.gui.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.models.CustomGradient;

public class AddCustomGradientFragment extends DialogFragment {

    private static final int SIZE = 100;
    AddCustomGradientListener listener;
    CustomGradient gradientPreview = new CustomGradient(
            new CustomGradient.HSV(0, 1, 1),
            new CustomGradient.HSV(360, 1, 1),
            1
    );
    private SeekBar start_h;
    private SeekBar start_s;
    private SeekBar start_v;
    private SeekBar end_h;
    private SeekBar end_s;
    private SeekBar end_v;
    private SeekBar waves;
    private ImageView preview;
    SeekBar.OnSeekBarChangeListener onHSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            updateSV();
            updatePreview();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    SeekBar.OnSeekBarChangeListener onSVSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            updatePreview();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddCustomGradientListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement AddCustomGradientListener");
        }
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_gradient, null);
        preview = view.findViewById(R.id.gradient_preview);

        start_h = view.findViewById(R.id.start_h);
        start_s = view.findViewById(R.id.start_s);
        start_v = view.findViewById(R.id.start_v);

        end_h = view.findViewById(R.id.end_h);
        end_s = view.findViewById(R.id.end_s);
        end_v = view.findViewById(R.id.end_v);

        waves = view.findViewById(R.id.gradient_waves);

        start_h.setOnSeekBarChangeListener(onHSeekBarChangeListener);
        start_s.setOnSeekBarChangeListener(onSVSeekBarChangeListener);
        start_v.setOnSeekBarChangeListener(onSVSeekBarChangeListener);

        end_h.setOnSeekBarChangeListener(onHSeekBarChangeListener);
        end_s.setOnSeekBarChangeListener(onSVSeekBarChangeListener);
        end_v.setOnSeekBarChangeListener(onSVSeekBarChangeListener);

        waves.setOnSeekBarChangeListener(onSVSeekBarChangeListener);

        CustomGradient fullRainbowGradient = new CustomGradient(new CustomGradient.HSV(0, 1, 1), new CustomGradient.HSV(360, 1, 1), 1);
        BitmapDrawable rainbowDrawable = new BitmapDrawable(getResources(), fullRainbowGradient.getBitmap(SIZE));
        start_h.setBackground(rainbowDrawable);
        end_h.setBackground(rainbowDrawable);

        updateSV();
        updatePreview();

        builder.setView(view)
                .setPositiveButton("Add", (dialog, id) -> {
                    listener.onCustomGradientAdded(gradientPreview);
                })
                .setNegativeButton("Cancel", (dialog, id) -> AddCustomGradientFragment.this.getDialog().cancel());


        return builder.create();
    }

    void updateSV() {
        CustomGradient startSDrawableGradient = new CustomGradient(
                new CustomGradient.HSV(start_h.getProgress(), 0, 1),
                new CustomGradient.HSV(start_h.getProgress(), 1, 1),
                1
        );

        BitmapDrawable startSDrawable = new BitmapDrawable(getResources(), startSDrawableGradient.getBitmap(SIZE));

        start_s.setBackground(startSDrawable);

        CustomGradient startVDrawableGradient = new CustomGradient(
                new CustomGradient.HSV(start_h.getProgress(), 1, 0),
                new CustomGradient.HSV(start_h.getProgress(), 1, 1),
                1
        );

        BitmapDrawable startVDrawable = new BitmapDrawable(getResources(), startVDrawableGradient.getBitmap(SIZE));

        start_v.setBackground(startVDrawable);


        CustomGradient endSDrawableGradient = new CustomGradient(
                new CustomGradient.HSV(end_h.getProgress(), 0, 1),
                new CustomGradient.HSV(end_h.getProgress(), 1, 1),
                1
        );

        BitmapDrawable endSDrawable = new BitmapDrawable(getResources(), endSDrawableGradient.getBitmap(SIZE));

        end_s.setBackground(endSDrawable);

        CustomGradient endVDrawableGradient = new CustomGradient(
                new CustomGradient.HSV(end_h.getProgress(), 1, 0),
                new CustomGradient.HSV(end_h.getProgress(), 1, 1),
                1
        );

        BitmapDrawable endVDrawable = new BitmapDrawable(getResources(), endVDrawableGradient.getBitmap(SIZE));

        end_v.setBackground(endVDrawable);
    }

    void updatePreview() {

        gradientPreview.setStartHSV(new CustomGradient.HSV(start_h.getProgress(), 0.01 * start_s.getProgress(), 0.01 * start_v.getProgress()));
        gradientPreview.setEndHSV(new CustomGradient.HSV(end_h.getProgress(), 0.01 * end_s.getProgress(), 0.01 * end_v.getProgress()));
        gradientPreview.setWaves(waves.getProgress() + 1);

        preview.setImageBitmap(gradientPreview.getBitmap(SIZE));
    }

    public interface AddCustomGradientListener {
        void onCustomGradientAdded(CustomGradient gradient);
    }
}