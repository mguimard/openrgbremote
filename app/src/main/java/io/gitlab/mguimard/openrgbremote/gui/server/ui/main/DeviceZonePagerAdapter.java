package io.gitlab.mguimard.openrgbremote.gui.server.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.models.DeviceZone;

public class DeviceZonePagerAdapter extends FragmentPagerAdapter {

    private final String ip;
    private final int port;
    private final ArrayList<DeviceZone> deviceZones;

    public DeviceZonePagerAdapter(Context context, FragmentManager fm, String ip, int port, ArrayList<DeviceZone> deviceZones) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.deviceZones = deviceZones;
        this.ip = ip;
        this.port = port;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        return DeviceZoneConfigFragment.newInstance(ip, port, deviceZones.get(position));
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        DeviceZone deviceZone = deviceZones.get(position);
        return deviceZone.getDevice().getName() + " (" + deviceZone.getZone().getName() + ")";
    }

    @Override
    public int getCount() {
        return deviceZones.size();
    }
}