package io.gitlab.mguimard.openrgbremote.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.models.CustomGradient;

public class GradientListAdapter extends ArrayAdapter<CustomGradient> {

    private final int size;
    private final ArrayList<CustomGradient> gradients;

    public GradientListAdapter(Context context, int textViewResourceId, ArrayList<CustomGradient> gradients, int size) {
        super(context, textViewResourceId, gradients);
        this.size = size;
        this.gradients = gradients;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getView(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NotNull ViewGroup parent) {
        return getView(convertView, position);
    }

    private View getView(View convertView, int position) {
        View view = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.palette_item, null);
                holder = new ViewHolder();
                holder.palette = view.findViewById(R.id.palette);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            CustomGradient gradient = getItem(position);
            holder.palette.setImageBitmap(gradient.getBitmap(size));

        } catch (Exception e) {

        }
        return view;
    }

    public CustomGradient getGradient(int position) {
        return gradients.get(position);
    }

    public static class ViewHolder {
        public ImageView palette;
    }
}