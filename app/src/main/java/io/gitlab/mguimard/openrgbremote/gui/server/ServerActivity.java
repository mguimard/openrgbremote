package io.gitlab.mguimard.openrgbremote.gui.server;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.mguimard.openrgb.entity.OpenRGBDevice;
import io.gitlab.mguimard.openrgb.entity.OpenRGBLed;
import io.gitlab.mguimard.openrgb.entity.OpenRGBMode;
import io.gitlab.mguimard.openrgb.entity.OpenRGBZone;
import io.gitlab.mguimard.openrgbremote.AppConstants;
import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.gui.server.ui.main.DeviceZonePagerAdapter;
import io.gitlab.mguimard.openrgbremote.models.DeviceZone;
import io.gitlab.mguimard.openrgbremote.services.ServerCommandRunner;

public class ServerActivity extends AppCompatActivity implements SaveProfileFragment.SaveProfileListener {

    public final static String IP_EXTRA = "IP_EXTRA";
    public final static String PORT_EXTRA = "PORT_EXTRA";
    private final ArrayList<DeviceZone> deviceZones = new ArrayList<>();
    TextView statusView;
    Toolbar toolbar;
    private String ip;
    private int port;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        Intent intent = getIntent();
        this.ip = intent.getStringExtra(IP_EXTRA);
        this.port = intent.getIntExtra(PORT_EXTRA, AppConstants.DEFAULT_PORT);

        toolbar = findViewById(R.id.server_toolbar);
        toolbar.setSubtitle(ip + ":" + port);
        setSupportActionBar(toolbar);


        statusView = findViewById(R.id.server_connect_status);

        ServerCommandRunner.getControllers(ip, port, (o, arg) -> {
            if (null == arg) {
                runOnUiThread(() -> {
                    statusView.setText("Sorry, server isn't available or has no controller detected.");
                });
            } else {

                int deviceIndex = 0;
                for (OpenRGBDevice device : (ArrayList<OpenRGBDevice>) arg) {
                    int zoneIndex = 0;
                    for (OpenRGBZone zone : device.getZones()) {
                        deviceZones.add(new DeviceZone(device, zone, deviceIndex, zoneIndex));
                        zoneIndex++;
                    }
                    deviceIndex++;
                }

                initUi(deviceZones);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.server_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_device_info:
                if (viewPager == null) {
                    return true;
                }
                deviceInfo(viewPager.getCurrentItem());
                return true;

            case R.id.action_save_profile:
                DialogFragment fragment = new SaveProfileFragment();
                fragment.show(getSupportFragmentManager(), null);
                return true;

            case R.id.action_load_profile:
                ServerCommandRunner.getProfileList(ip, port, (o, arg) -> {
                    showProfileDialog((String[]) arg);
                });
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void showProfileDialog(final String[] profiles) {
        runOnUiThread(() -> {
            AlertDialog.Builder loadProfileDialogBuilder = new AlertDialog.Builder(this);
            loadProfileDialogBuilder.setTitle("Load profile");

            loadProfileDialogBuilder.setItems(profiles, (dialog, which) -> {
                ServerCommandRunner.loadProfile(ip, port, profiles[which]);
            });

            loadProfileDialogBuilder.setNegativeButton("Close", (dialog, which) -> {
                dialog.dismiss();
            });

            AlertDialog loadProfileDialog = loadProfileDialogBuilder.create();
            loadProfileDialog.show();
        });
    }

    @Override
    public void onProfileNameChosen(String name) {
        ServerCommandRunner.saveProfile(ip, port, name + ".orp");
    }

    void initUi(ArrayList<DeviceZone> deviceZones) {
        runOnUiThread(() -> {
            statusView.setVisibility(View.GONE);
            DeviceZonePagerAdapter deviceZonePagerAdapter =
                    new DeviceZonePagerAdapter(ServerActivity.this, getSupportFragmentManager(), ip, port, deviceZones);
            viewPager = findViewById(R.id.view_pager);
            viewPager.setAdapter(deviceZonePagerAdapter);
            TabLayout tabs = findViewById(R.id.tabs);
            tabs.setupWithViewPager(viewPager);
        });
    }

    private void deviceInfo(int deviceZoneIndex) {
        OpenRGBDevice device = deviceZones.get(deviceZoneIndex).getDevice();

        LayoutInflater factory = LayoutInflater.from(this);
        final View deviceInfoView = factory.inflate(R.layout.dialog_device_info, null);
        final AlertDialog deviceInfoDialog = new AlertDialog.Builder(this).create();
        deviceInfoDialog.setView(deviceInfoView);

        ((TextView) deviceInfoView.findViewById(R.id.device_info_name)).setText(device.getName());
        ((TextView) deviceInfoView.findViewById(R.id.device_vendor_value)).setText(device.getVendor());
        ((TextView) deviceInfoView.findViewById(R.id.device_type_value)).setText(device.getType().toString());
        ((TextView) deviceInfoView.findViewById(R.id.device_desc_value)).setText(device.getDesc());
        ((TextView) deviceInfoView.findViewById(R.id.device_version_value)).setText(device.getVersion());
        ((TextView) deviceInfoView.findViewById(R.id.device_serial_value)).setText(device.getSerial());
        ((TextView) deviceInfoView.findViewById(R.id.device_location_value)).setText(device.getLocation());

        String sep = "\n";
        List<String> ledNames = new ArrayList<>();
        for (OpenRGBLed led : device.getLeds()) {
            ledNames.add(led.getName());
        }
        ((TextView) deviceInfoView.findViewById(R.id.device_leds_value)).setText(StringUtils.join(ledNames, sep));

        List<String> modeNames = new ArrayList<>();
        for (OpenRGBMode mode : device.getModes()) {
            modeNames.add(mode.getName());
        }
        ((TextView) deviceInfoView.findViewById(R.id.device_modes_value)).setText(StringUtils.join(modeNames, sep));

        List<String> zoneNames = new ArrayList<>();
        for (OpenRGBZone zone : device.getZones()) {
            zoneNames.add(zone.getName());
        }
        ((TextView) deviceInfoView.findViewById(R.id.device_zones_value)).setText(StringUtils.join(zoneNames, sep));

        deviceInfoDialog.show();
    }

    private void loadProfile() {

        ServerCommandRunner.getProfileList(ip, port, (o, arg) -> {
            AlertDialog.Builder loadProfileDialogBuilder = new AlertDialog.Builder(this);
            loadProfileDialogBuilder.setTitle("Load profile");
            String[] profiles = (String[]) arg;
            //final String[] profiles =  new String[]{"Android Development", "Web Development", "Machine Learning"};

            loadProfileDialogBuilder.setSingleChoiceItems(profiles, 0, (dialog, which) -> {
                // which = profile index
                dialog.dismiss();
            });

            loadProfileDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> {

            });

            AlertDialog loadProfileDialog = loadProfileDialogBuilder.create();
            loadProfileDialog.show();

        });


    }

}