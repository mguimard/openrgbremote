package io.gitlab.mguimard.openrgbremote.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.models.OpenRGBServer;

public class ServerListAdapter extends ArrayAdapter<OpenRGBServer> {

    private static LayoutInflater inflater = null;
    private ArrayList<OpenRGBServer> servers;

    public ServerListAdapter(Activity activity, int textViewResourceId, ArrayList<OpenRGBServer> servers) {
        super(activity, textViewResourceId, servers);
        try {
            this.servers = servers;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }

    public int getCount() {
        return servers.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.server_list_item, null);
                holder = new ViewHolder();
                holder.display_name = vi.findViewById(R.id.server_name);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            OpenRGBServer server = servers.get(position);
            holder.display_name.setText(server.getDisplayName());

        } catch (Exception e) {

        }
        return vi;
    }

    public OpenRGBServer getServer(int position) {
        return servers.get(position);
    }

    public static class ViewHolder {
        public TextView display_name;
    }
}