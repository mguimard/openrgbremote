package io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgb.entity.OpenRGBColor;
import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.adapters.GradientListAdapter;
import io.gitlab.mguimard.openrgbremote.models.CustomGradient;
import io.gitlab.mguimard.openrgbremote.services.CustomGradientsManager;

public class GradientColorChooserDialogFragment extends DialogFragment {

    private final ArrayList<CustomGradient> gradients = new ArrayList<>();
    private final GradientColorChooserListener listener;
    private final int size;

    public GradientColorChooserDialogFragment(int size, GradientColorChooserListener listener) {
        this.listener = listener;
        this.size = size;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);

        CustomGradientsManager customGradientsManager = CustomGradientsManager.getInstance(getContext());
        this.gradients.addAll(customGradientsManager.getCustomGradientList().getGradients());

        this.gradients.add(new CustomGradient(CustomGradient.green, CustomGradient.red, 1));
        this.gradients.add(new CustomGradient(CustomGradient.red, CustomGradient.green, 1));

        this.gradients.add(new CustomGradient(CustomGradient.blue, CustomGradient.green, 1));
        this.gradients.add(new CustomGradient(CustomGradient.green, CustomGradient.blue, 1));

        this.gradients.add(new CustomGradient(CustomGradient.blue, CustomGradient.red, 1));
        this.gradients.add(new CustomGradient(CustomGradient.red, CustomGradient.blue, 1));
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choose_gradient_colors, null);

        ListView paletteListView = view.findViewById(R.id.palette_list);
        GradientListAdapter adapter = new GradientListAdapter(getContext(), R.layout.palette_item, gradients, size);
        paletteListView.setAdapter(adapter);

        paletteListView.setOnItemClickListener((parent, view1, position, id) -> listener.onGradientChanged(adapter.getItem(position).toColors(size)));

        builder.setView(view)
                .setNegativeButton("Close", (dialog, id) -> GradientColorChooserDialogFragment.this.getDialog().cancel());
        return builder.create();
    }

    public interface GradientColorChooserListener {
        void onGradientChanged(OpenRGBColor[] values);
    }
}