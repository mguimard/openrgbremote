package io.gitlab.mguimard.openrgbremote.gui.custom;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.adapters.GradientListAdapter;
import io.gitlab.mguimard.openrgbremote.models.CustomGradient;
import io.gitlab.mguimard.openrgbremote.services.CustomGradientsManager;

public class CustomGradientsActivity extends AppCompatActivity implements AddCustomGradientFragment.AddCustomGradientListener {

    CustomGradientsManager customGradientsManager;
    GradientListAdapter gradientListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_gradients);
        customGradientsManager = CustomGradientsManager.getInstance(this);

        findViewById(R.id.add_gradient)
                .setOnClickListener(v -> showDialog());

        gradientListAdapter = new GradientListAdapter(this, 0,
                customGradientsManager.getCustomGradientList().getGradients(), 100);

        ListView gradientListView = findViewById(R.id.gradient_list_view);
        gradientListView.setAdapter(gradientListAdapter);

        gradientListView.setOnItemLongClickListener((parent, view, position, id) -> {
            customGradientsManager.removeCustomGradient(gradientListAdapter.getGradient(position));
            updateUI();
            return false;
        });

        updateUI();
    }

    @Override
    public void onCustomGradientAdded(CustomGradient gradient) {
        customGradientsManager.addCustomGradient(gradient);
        updateUI();
    }

    void showDialog() {
        DialogFragment fragment = new AddCustomGradientFragment();
        fragment.show(getSupportFragmentManager(), null);
    }

    void updateUI() {
        boolean isEmpty = customGradientsManager.isEmpty();
        findViewById(R.id.not_empty_gradient_list).setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        findViewById(R.id.empty_gradient_list).setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        gradientListAdapter.notifyDataSetChanged();

    }
}