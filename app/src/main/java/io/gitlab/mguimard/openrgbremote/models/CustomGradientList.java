package io.gitlab.mguimard.openrgbremote.models;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CustomGradientList {
    private ArrayList<CustomGradient> gradients;

    public CustomGradientList(ArrayList<CustomGradient> gradients) {
        this.gradients = gradients;
    }

    public boolean isEmpty() {
        return gradients.isEmpty();
    }

    public boolean add(CustomGradient customGradient) {
        return gradients.add(customGradient);
    }

    public boolean remove(@Nullable Object o) {
        return gradients.remove(o);
    }

    public ArrayList<CustomGradient> getGradients() {
        return gradients;
    }
}
