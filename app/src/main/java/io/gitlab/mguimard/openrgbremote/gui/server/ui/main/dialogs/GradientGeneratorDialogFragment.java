package io.gitlab.mguimard.openrgbremote.gui.server.ui.main.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;

public class GradientGeneratorDialogFragment extends DialogFragment {

    public GradientGeneratorDialogFragment() {
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choose_gradient_colors, null);


        builder.setView(view)
                .setNegativeButton("Close", (dialog, id) -> GradientGeneratorDialogFragment.this.getDialog().cancel());
        return builder.create();
    }

}