package io.gitlab.mguimard.openrgbremote.gui.server;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;

public class SaveProfileFragment extends DialogFragment {

    SaveProfileListener listener;

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            listener = (SaveProfileListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement NoticeDialogListener");
        }
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_save_profile, null);
        builder.setView(view)
                .setPositiveButton("Save", (dialog, id) -> {
                    String profileName = ((EditText) view.findViewById(R.id.save_profile_name)).getText().toString().trim();
                    if (!profileName.isEmpty()) {
                        listener.onProfileNameChosen(profileName);
                        dismiss();
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> SaveProfileFragment.this.getDialog().cancel());
        return builder.create();
    }

    public interface SaveProfileListener {
        void onProfileNameChosen(String name);
    }
}