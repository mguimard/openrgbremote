package io.gitlab.mguimard.openrgbremote.gui.home;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import io.gitlab.mguimard.openrgbremote.R;
import io.gitlab.mguimard.openrgbremote.models.OpenRGBServer;

public class AddServerFragment extends DialogFragment {

    AddServerListener listener;

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddServerListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement NoticeDialogListener");
        }
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_server, null);
        builder.setView(view)
                .setPositiveButton("Add", (dialog, id) -> {
                    String ip = ((EditText) view.findViewById(R.id.add_server_ip)).getText().toString();
                    int port = Integer.parseInt(((EditText) view.findViewById(R.id.add_server_port)).getText().toString());
                    OpenRGBServer server = new OpenRGBServer(ip, port);
                    listener.onServerAdded(server);
                })
                .setNegativeButton("Cancel", (dialog, id) -> AddServerFragment.this.getDialog().cancel());
        return builder.create();
    }

    public interface AddServerListener {
        void onServerAdded(OpenRGBServer server);
    }
}