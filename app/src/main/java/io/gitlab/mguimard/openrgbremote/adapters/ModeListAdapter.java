package io.gitlab.mguimard.openrgbremote.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.gitlab.mguimard.openrgb.entity.OpenRGBMode;
import io.gitlab.mguimard.openrgbremote.R;

public class ModeListAdapter extends ArrayAdapter<OpenRGBMode> {

    public ModeListAdapter(Context context, int textViewResourceId, int textViewId, ArrayList<OpenRGBMode> modes) {
        super(context, textViewResourceId, textViewId, modes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getView(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NotNull ViewGroup parent) {
        return getView(convertView, position);
    }

    private View getView(View convertView, int position) {
        View view = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.mode_spinner_item, null);
                holder = new ViewHolder();
                holder.mode_name = view.findViewById(R.id.mode_name);
                holder.mode_color_mode = view.findViewById(R.id.mode_color_mode);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            OpenRGBMode mode = getItem(position);

            holder.mode_name.setText(mode.getName());
            holder.mode_color_mode.setText(mode.getColorMode().getName());

        } catch (Exception e) {

        }
        return view;
    }

    public static class ViewHolder {
        public TextView mode_name;
        public TextView mode_color_mode;
    }
}