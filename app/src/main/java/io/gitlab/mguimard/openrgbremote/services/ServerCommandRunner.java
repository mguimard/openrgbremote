package io.gitlab.mguimard.openrgbremote.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import io.gitlab.mguimard.openrgb.client.OpenRGBClient;
import io.gitlab.mguimard.openrgb.entity.OpenRGBColor;
import io.gitlab.mguimard.openrgb.entity.OpenRGBDevice;
import io.gitlab.mguimard.openrgb.entity.OpenRGBMode;
import io.gitlab.mguimard.openrgbremote.AppConstants;

public class ServerCommandRunner {

    public static Observable getControllers(String ip, int port, Observer o) {
        ControllerData controllerData = new ControllerData();
        controllerData.addObserver(o);

        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            ArrayList<OpenRGBDevice> devices = new ArrayList<>();

            try {
                client.connect();
                int controllerCount = client.getControllerCount();
                for (int i = 0; i < controllerCount; i++) {
                    devices.add(client.getDeviceController(i));
                }
                controllerData.onFinished(devices);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
                controllerData.onFinished(null);
            }

        }).start();

        return controllerData;
    }

    public static void setMode(String ip, int port, int deviceIndex, int modeIndex, OpenRGBMode mode) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.updateMode(deviceIndex, modeIndex, mode);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public static void setLEDs(String ip, int port, int deviceIndex, OpenRGBColor[] colors) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.updateLeds(deviceIndex, colors);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public static void setZoneLEDs(String ip, int port, int deviceIndex, int zoneIndex, OpenRGBColor[] colors) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.updateZoneLeds(deviceIndex, zoneIndex, colors);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public static void resizeZone(String ip, int port, int deviceIndex, int zoneIndex, int size) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.resizeZone(deviceIndex, zoneIndex, size);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public static void saveProfile(String ip, int port, String profileName) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.saveProfile(profileName);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public static void getProfileList(String ip, int port, Observer o) {
        ProfileData profileData = new ProfileData();
        profileData.addObserver(o);

        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                String[] profileList = client.getProfileList();
                profileData.onFinished(profileList);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
                profileData.onFinished(new String[0]);
            }

        }).start();
    }

    public static void loadProfile(String ip, int port, String profileName) {
        OpenRGBClient client = new OpenRGBClient(ip, port, AppConstants.DEFAULT_NAME);

        new Thread(() -> {
            try {
                client.connect();
                client.loadProfile(profileName);
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    static class ControllerData extends Observable {
        public void onFinished(ArrayList<OpenRGBDevice> devices) {
            setChanged();
            notifyObservers(devices);
        }
    }

    static class ProfileData extends Observable {
        public void onFinished(String[] profiles) {
            setChanged();
            notifyObservers(profiles);
        }
    }
}
